'use strict';

module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  router.get('/', server.loopback.status());
  server.use(router);
  var http = require('http');
  var kafka = require('kafka-node');
  var Producer = kafka.Producer;
  var KeyedMessage = kafka.KeyedMessage;
  var client = new kafka.KafkaClient({kafkaHost: '34.209.78.89:9092'});

  var producer = new Producer(client);

  producer.on('ready', function() { });

  producer.on('error', function(err) { });
  router.get('/getStreaming', function(req, res) {
    var http = require('http');
    var kafka = require('kafka-node');
    var Producer = kafka.Producer;
    var KeyedMessage = kafka.KeyedMessage;
    var client = new kafka.KafkaClient({kafkaHost: '34.209.78.89:9092'});

    var producer = new Producer(client);

    producer.on('ready', function() { });

    producer.on('error', function(err) { });

    // function sendData() {
    // Generate random number between 5 and 10
    // Math.floor(Math.random()*(max-min+1)+min);
    // var datapoint = Math.floor(Math.random() * 6 + 5).toString();
    var randomnumber = Math.floor(Math.random() * 100) + 1;
    var randomnumber1 = Math.floor(Math.random() * 99) + 1;
    var randomnumber2 = Math.floor(Math.random() * 98) + 1;
    var randomnumber4 = Math.floor(Math.random() * 97) + 1;

    var waterLevel = server.models.WaterLevel;
    // var waterLevelObj = {
    //   'pressureInput': rando,
    //   'idealOutput': ideal,
    //   'sensorOutput': sensor,
    // };
    // console.log('waterrrrrrrrr ', waterLevelObj);
    var multi = [
      {
        'name': randomnumber,
        'value': randomnumber1,
      },
      {
        'name': randomnumber2,
        'value': randomnumber4,
      },

    ];
    var obj = {
      'name': 'Water',
      'series': multi,
    };
    var payloads = [
      {topic: 'afzal', messages: obj},
    ];
    producer.send(payloads, function(err, data) {
      // console.log(err, waterLevelObj);
      // res.send(data);
      // res.setHeader('name', data);
      res.writeContinue();
      res.send(obj);
      // res.writeHead(200);
      // res.writeHead(200, {
      //   'Content-Length': Buffer.byteLength(data),
      //   'Content-Type': 'text/plain',
      // });
      // req.setHeader('Content-Type', 'application/json');
      // res.end();
    });
    // }
    // setInterval(sendData, 1000);// send Data every 1 second
  });
  router.get('/getHeat', function(req, res) {
    var http = require('http');
    var kafka = require('kafka-node');
    var Producer = kafka.Producer;
    var KeyedMessage = kafka.KeyedMessage;
    var client = new kafka.KafkaClient({kafkaHost: '34.209.78.89:9092'});

    var producer = new Producer(client);

    producer.on('ready', function() { });

    producer.on('error', function(err) { });
    var randomnumber = Math.floor(Math.random() * 100) + 1;

    var multi = [
      {
        'name': 'Heat',
        'series': [
          {
            'name': '',
            'value': 894000 + randomnumber,
          },
        ],
      },

    ];
    var obj = {
      'name': 'Water',
      'series': multi,
    };
    var payloads = [
      {topic: 'water', messages: obj},
    ];
    producer.send(payloads, function(err, data) {
      console.log('datadatadatadata ', data);
      res.writeContinue();
      res.send(multi);
    });
  });
  router.get('/getPressure', function(req, res) {
    var http = require('http');
    var kafka = require('kafka-node');
    var Producer = kafka.Producer;
    var KeyedMessage = kafka.KeyedMessage;
    var client = new kafka.KafkaClient({kafkaHost: '34.209.78.89:9092'});

    var producer = new Producer(client);

    producer.on('ready', function() { });

    producer.on('error', function(err) { });
    var randomnumber = Math.floor(Math.random() * 50) + 1;

    var multi = [
      {
        'name': 'Pressure',
        'value': 80 + randomnumber,
      },
    ];
    var obj = {
      'name': 'Pressure',
      'series': multi,
    };
    var payloads = [
      {topic: 'pressure', messages: obj},
    ];
    producer.send(payloads, function(err, data) {
      console.log('pressure-------------------------- ', multi);
      res.writeContinue();
      res.send(multi);
    });
  });
  router.get('/getWind', function(req, res) {
    var randomnumber = Math.random() * (1.5 - 1) + 1;
    var randomnumber1 = Math.floor(Math.random() * 11000) + 1;
    var randomnumber2 = Math.floor(Math.random() * 12000) + 1;

    var multi = [
      ['Label', 'Value'],
      ['Value', 2.78 + randomnumber],
    ];
    var obj = {
      'name': 'Wind',
      'series': multi,
    };
    var payloads = [
      {topic: 'wind', messages: obj},
    ];
    producer.send(payloads, function(err, data) {
      console.log('getWind ', multi);
      res.writeContinue();
      res.send(multi);
    });
  });
  router.get('/getWaterLevel', function(req, res) {
    var randomnumber = Math.random() * (150 - 1) + 1;
    var randomnumber1 = Math.floor(Math.random() * 11000) + 1;
    var randomnumber2 = Math.floor(Math.random() * 12000) + 1;

    var multi = [
      ['Time', 'water'],
      ['2', 510 + randomnumber],
      ['3', 110 + randomnumber],
      ['4', 640 + randomnumber],
      ['5', 130 + randomnumber],
    ];
    var obj = {
      'name': 'Wind',
      'series': multi,
    };
    var payloads = [
      {topic: 'water Level', messages: obj},
    ];
    producer.send(payloads, function(err, data) {
      console.log('getWind ', multi);
      res.writeContinue();
      res.send(multi);
    });
  });

  router.get('/getHumidity', function(req, res) {
    var randomnumber = Math.floor(Math.random() * 50) + 1;

    var multi = [
      {
        'name': 'Humidity',
        'value': 80 + randomnumber,
      },
    ];
    var obj = {
      'name': 'Humidity',
      'series': multi,
    };
    var payloads = [
      {topic: 'humidity', messages: obj},
    ];
    producer.send(payloads, function(err, data) {
      console.log('Humidity-------------------------- ', multi);
      res.writeContinue();
      res.send(multi);
    });
  });
};
