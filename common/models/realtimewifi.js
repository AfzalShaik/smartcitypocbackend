'use strict';

module.exports = function (Realtimewifi) {

    Realtimewifi.test = function (data, callback) {
        var request = require('request');
        // Realtimewifi.destroyAll({}, function (err) {            
        // });
        request('https://damzgreurb.execute-api.us-east-1.amazonaws.com/iotprod/id/1', function (error, response, body) {
            let dataaa = JSON.parse(body)
            Realtimewifi.create(dataaa, function (err, results) {
                if (err) return callback(err);
                var obj = {};
                obj.data = results;
                callback(null, obj); 
            });
        });

    }
    Realtimewifi.remoteMethod('test', {
        description: 'Read the File & update data to DB',
        returns: {
            root: true,
            type: 'object',
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/test',
            verb: 'get',
        },
    });

};
