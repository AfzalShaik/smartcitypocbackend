'use strict';

module.exports = function(Waterlevel) {
    Waterlevel.getWebsocket = function(id, cb) {
var WebSocketClient = require('websocket').client;
var ws = new WebSocketClient();
ws.connect('ws://124.153.85.159:8080');

ws.on('connect', function(connection) {
    // var WebSocketClient = require('websocket').client;
    console.log('connected!');

    connection.on('close', function(reasonCode, description) {
        console.log('connection closed: ' + reasonCode + ' - ' + description);
    });

    connection.on('error', function(err) {
        console.log('connection error: ' + err.stack);
    });

    connection.on('frame', function(frame) {
        console.log('frame: ' + JSON.stringify(frame));
    }); 

    connection.on('message', function(message) {
        console.log('message: ' + JSON.stringify(message));
    });

    console.log('send index messages');
    connection.send(getIndexMessage(1));
    connection.send(getIndexMessage(2));

    console.log('send flush message');
    connection.send(getFlushMessage());
});

ws.on('connectFailed', function(err) {
    console.log('connection failed: ' + err.stack);
});
}

// remote method declarations.
    Waterlevel.remoteMethod('getWebsocket', {
        description: 'To getWebsocket details',
        returns: {
          type: 'array',
          root: true,
        },
        accepts: {
          arg: 'id ',
          type: 'number',
          http: {
            source: 'query',
          },
        },
        http: {
          path: '/getWebsocket',
          verb: 'get',
        },
      });
};
