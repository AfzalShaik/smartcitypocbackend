var server = require('../../server/server');
module.exports = function(Request) {
  Request.observe('before save', function (ctx, next) {

    if(ctx.data!=undefined && ctx.data!=null){
      ctx.data['updatedTime']=new Date();
      next();
    }
    else {
          checkUniqueId(ctx,next);
      ctx.instance.createdTime=new Date();
      next();
    }
  });
  function checkUniqueId(ctx,next){
    var randomstring = require("randomstring");
    var uniqueId = randomstring.generate({
      length: 7,
      charset: 'alphanumeric'
    });
    var date=new Date();
    var month;
    var dateIS;
    if((date.getMonth()+1)<10){
      month='0'+(date.getMonth()+1);
    }else{
      month=(date.getMonth()+1);
    }
    if(date.getDate()<10){
      dateIS='0'+date.getDate();
    }else{
      dateIS=date.getDate();
    }
    var string=date.getFullYear()+''+month+dateIS;
    console.log('date is '+date.getFullYear()+'data'+string);
    uniqueId=string+uniqueId;
  }
};
