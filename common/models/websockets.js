'use strict';

module.exports = function(Websockets) {
    Websockets.getWebsocket = function(data, cb) {
        Websockets.find({}, function(findErr, findOut) {
            if(findOut) {
                var arr = [];
                var response = [];
                for(var j = 0; j<findOut.length; j++) {
                    arr = findOut[j].controllers;
                    for(var i = 0; i< arr.length; i++) {
                        if (arr[i].id == 2309) {
                         response.push(arr[i]);
                        }
                    }
                }
               var obj = {};
               obj.data = response;
               cb(null, obj);
               response = [];
               arr = [];
            }
        })
    }
    Websockets.remoteMethod('getWebsocket', {
        description: 'Read the File & update data to DB',
        returns: {
            root: true,
            type: 'object',
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/getWebsocket',
            verb: 'get',
        },
    });
};
