'use strict';

module.exports = function(Bulksms) {
    Bulksms.getAnnouncement = function(data, cb) {
        // console.log('dataaaaaaaa ', data);
        var request = require('request');
        // console.log('http://125.17.215.3/vms/api/vmsdata/index/172.19.71.48/5000/' + data.message);
        request('http://125.17.215.2:8080/ECB/rest/synchronize/announce?announce_Audio=disaster&announce_destination=barix_p', function (error, response, body) {
            request('http://125.17.215.3/vms/api/vmsdata/index/172.19.71.48/5000/CongestionAtTankBundRoad', function (error1, response1, body1) {
               
            cb(null, 'done');
            });
        });
    }
    Bulksms.remoteMethod('getAnnouncement', {
        description: 'Read the File & update data to DB',
        returns: {
            root: true,
            type: 'object',
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/getAnnouncement',
            verb: 'get',
        },
    });
};
