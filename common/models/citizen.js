'use strict';

module.exports = function(Citizen) {


    Citizen.afterRemote('login',function(ctx,modelInstance,next){

        Citizen.findById(ctx.result.userId, function (err, data) {
            if (!err) {
                ctx.result['citizen'] = data;
                next();
            } else {
                next('Login Failed', null);
            }
        })
    })
};
