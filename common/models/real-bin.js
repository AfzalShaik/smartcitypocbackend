'use strict';

module.exports = function (Realbin) {
    Realbin.getBinData = function (id, cb) {
        var request = require('request');
        request('http://52.26.153.39:8080/smartbhopal/allbindevices', function (error, response, body) {
            
            var resp = body;
            resp = JSON.parse(body)

            // Realbin.destroyAll({}, function(destroyErr, destroyOut) {
                // if (destroyOut) {
                    Realbin.create(resp, function(createErr, createResp) {
                        // console.log('create respv bb', resp);
                        cb(null, createResp);
                    });
                // }
            // })
        });
    }
    Realbin.remoteMethod('getBinData', {
        description: 'To getBinData details',
        returns: {
            type: 'array',
            root: true,
        },
        accepts: {
            arg: 'id ',
            type: 'number',
            http: {
                source: 'query',
            },
        },
        http: {
            path: '/getBinData',
            verb: 'get',
        },
    });
};
