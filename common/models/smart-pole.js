'use strict';

module.exports = function(Smartpole) {

    Smartpole.SmartPoleData = function (data, callback) {
        var request = require('request');
        // Smartpole.destroyAll({}, function (err) {            
        // });
        request('https://0dl4zg1kwi.execute-api.us-east-1.amazonaws.com/iotlightpole/id/1', function (error, response, body) {
            let dataaa = JSON.parse(body)
            Smartpole.create(dataaa, function (err, results) {
                if (err) return callback(err);
                var obj = {};
                obj.data = results;
                callback(null, obj); 
            });
        });

    }
    Smartpole.remoteMethod('SmartPoleData', {
        description: 'Read the File & update data to DB',
        returns: {
            root: true,
            type: 'object',
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/SmartPoleData',
            verb: 'get',
        },
    });
    Smartpole.getRealPole = function(data, cb) {
        var request = require('request');
        var headers = { ApiKey : 'hq0h0J/lzHgcg55wzFIwYw=='};
        var options2 = {
            url: 'http://54.197.242.112/LGAPI/Cimcon/Commands/State',// + accesstoken2,
            method: 'POST',
            body: data,
            json: true,   // <--Very important!!!
            headers: headers
        }
        request(options2, function (error, response, body) {
            cb(null, body);
        });
    }
    Smartpole.remoteMethod('getRealPole', {
        description: 'Read the File & update data to DB',
        returns: {
            root: true,
            type: 'object',
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/getRealPole',
            verb: 'get',
        },
    });


};