'use strict';

module.exports = function(Citizencomplaints) {

    Citizencomplaints.observe('before save',function(ctx,next){
        if (ctx.isNewInstance) {
            ctx.instance['status']="NEW"
            ctx.instance['createdTime']=new Date()
            next();
        }else{
            next();

        }
    })
};
