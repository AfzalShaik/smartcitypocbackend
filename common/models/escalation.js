'use strict';
var server = require('../../server/server');
module.exports = function(Escalation) {
    Escalation.sendEmail = function(data, callBc) {
       var email = server.models.Email;
        email.send({
          to: data.email,
          from: 'operator.coimbatore@gmail.com',
          subject: 'Escalation Mail',
          text: data.text,
          // html: data.message,
        }, function(err, mail) {
          if (err) {
            callBc(err, null);
          } else {
            callBc(null, data);
          }
        });
      };
      Escalation.remoteMethod('sendEmail', {
        description: 'Send Email',
        returns: {
          type: 'array',
          root: true,
        },
        accepts: [{
          arg: 'data',
          type: 'object',
          required: true,
          http: {
            source: 'body',
          },
        }],
        http: {
          path: '/sendEmail',
          verb: 'post',
        },
      });
};
