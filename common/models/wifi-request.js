'use strict';

const random = require('randomatic');
var server = require('../../server/server')

module.exports = function (Wifirequest) {

  Wifirequest.observe('before save', function (ctx, next) {

    if (ctx.isNewInstance) {
      const citizen = server.models.citizen;
      var otp = random('0123456789', 4);
      const sms = server.models.sms;


      ctx.instance['otp'] = otp;

      citizen.findById(ctx.instance.reqBy, function (err, data) {
        if (err) {
          next(err)
        } else {
          var params = {
            message: otp + ' is otp to connect your wifi',

            dest: [data.mobileNumber]
          }
          sms.create(params, function (err, data) {
            if (err) {
              next(err)
            } else {
              next()
            }
          })
        }

      })


    } else {
      if (ctx.data.otp) {
        if (ctx.currentInstance.otp == ctx.data.otp) {
          next(null, "success")
        } else {
          next("error")
        }
      } else {
        next()
      }
    }
  })

};