'use strict';

module.exports = function (Csvfile) {

    Csvfile.readFile = function (data, callBc) {
        var arr = [];
        var csv = require("fast-csv");

        csv
            .fromPath("./CsvFile/Bhopal_Parking_List.csv", {headers: true})  
            .on("data", function (data) {                
                arr.push(data);           
            })
            .on("end", function () {
                Csvfile.create(arr, function(err, resp) {
                });
                callBc(null, "Data Updated.......");
            });
    };
    Csvfile.remoteMethod('readFile', {
        description: 'Read the File & update data to DB',
        returns: {
            type: 'array'
        },
        accepts: [{
            arg: 'data',
            type: 'object',
            http: {
                source: 'body',
            },
        }],
        http: {
            path: '/readFile',
            verb: 'get',
        },
    });

// api's data
Csvfile.apiCalls = function(data, callBc) {
    var request = require('request');
    // request.post('https://ems.myairmyhealth.com/airengine/no-auth', {"userName" : "esfl_paqs", "password":"bddf1f90#P"}, function (error, response, body) {
    //     console.log('bodyyyyyyyyyyyyy', body);
    //     callBc(null, body);
    //     var options = {
    //         url: 'https://ems.myairmyhealth.com/airengine/v5/pollutiondata/lastdata/834e9584-1ca7-4c57-88e8-c588b75e867d',
    //         headers: {
    //           'User-Agent': 'request'
    //         }
    //       };
    // });
    request.post({
        headers: {'content-type' : 'application/json'},
        url:     'https://ems.myairmyhealth.com/airengine/no-auth',
        body:    {"userName" : "esfl_paqs",
        "password":"bddf1f90#P"
    }
      }, function(error, response, body){
        console.log(body);
      });
}
Csvfile.remoteMethod('apiCalls', {
    description: 'API data reading and update data to DB',
    returns: {
        type: 'array'
    },
    accepts: [{
        arg: 'data',
        type: 'object',
        http: {
            source: 'body',
        },
    }],
    http: {
        path: '/apiCalls',
        verb: 'get',
    },
});
};
