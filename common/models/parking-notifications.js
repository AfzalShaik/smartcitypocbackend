'use strict';

var server = require('../../server/server')

module.exports = function (Parkingnotifications) {

  // Parkingnotifications.observe('before save', function (ctx, next) {
  //   if (!ctx.isNewInstance) {
  //     ctx.data['updatedBy'] = new Date().toDateString();
  //     if (ctx.data.status) {
  //       if (ctx.data.status == 'completed') {

  //         var raisedBy = ctx.currentInstance.raisedBy.toString();
  //         const sms = server.models.sms;
  //         const citizen = server.models.citizen;

  //         console.log('raised By data ' + raisedBy);

  //         citizen.findById(raisedBy, function (err, data) {
  //           if (err) {
  //             next(err)
  //           } else {
  //             var params = {
  //               message: 'Nearest Parking slot location is sent to your Notification Tab Under Smart Parking',
  //               dest: [data.mobileNumber]
  //             }
  //             sms.create(params, function (err, data) {
  //               if (err) {
  //                 next(err)
  //               } else {
  //                 next()
  //               }
  //             })
  //           }

  //         })

  //       } else {
  //         next()
  //       }
  //     } else {
  //       next()
  //     }
  //   } else {
  //     ctx.instance['createdBy'] = new Date().toDateString();
  //     ctx.instance['updatedBy'] = new Date().toDateString();
  //     next();
  //   }
  // })
  Parkingnotifications.departMentResponse = function (notId, citizenId, callback) {

    Parkingnotifications.findOne({
      where: {
        id: notId
      }
    }, function (err, data) {
      if (err) {
        callback(err)
      } else {
        console.log('data' + JSON.stringify(data))

        data.updateAttribute('status', 'completed');
        const sms = server.models.sms;
        const citizen = server.models.citizen;
        citizen.findById(citizenId, function (err, data) {
          if (err) {
            callback(err)
          } else {
            var params = {
              message: 'Nearest Parking slot location is sent to your Notification Tab Under Smart Parking',
              dest: [data.mobileNumber]
            }
            sms.create(params, function (err, data) {
              if (err) {
                callback(err)
              } else {
                callback(null, "success")
              }
            })
          }

        })
      }
    })
  };
};
